Introduction
-------------
Proof of concept of backup camera like functionality using two android phones based on wifi direct using Java at first and Then Kotlin.


Setup 
-------------
![BackupCameraPlus](/uploads/1ca70fb713a2203d5763cfb021179ae6/BackupCameraPlus.jpg)
![BackupCameraPlusSetup](/uploads/bb5edd4e31a6d15a051ae3232e7b7c0a/BackupCameraPlusSetup.jpg)

Demonstration
-------------
![BackupCameraPlusView](/uploads/72662958bb186b0f4b78bacd733bc955/BackupCameraPlusView.jpg)

Next Steps
---------
- Refactoring
- TDD
- Android Ops
- Issues
- Live Camera View Transfer
- driver assistance features

Learning 
------------
- Wifi Direct Demo :  https://android.googlesource.com/platform/development/+/master/samples/WiFiDirectDemo
- https://medium.com/swlh/introduction-to-androids-camerax-with-java-ca384c522c5
- https://source.android.com/setup/build/downloading#using-authentication


Credits
-------
Refresh Image Courtesy : https://pixabay.com/illustrations/icon-reload-refresh-redo-load-1968246/
