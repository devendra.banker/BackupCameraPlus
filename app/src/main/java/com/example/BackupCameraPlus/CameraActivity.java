package com.example.BackupCameraPlus;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.util.Size;
import android.view.OrientationEventListener;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.CameraX;
import androidx.camera.core.FocusMeteringAction;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.ImageProxy;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;

import com.google.common.util.concurrent.ListenableFuture;

import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

public class CameraActivity extends AppCompatActivity {

    private PreviewView previewView;
    private ListenableFuture<ProcessCameraProvider> cameraProviderFuture;
    private TextView textView;

    //TODO : Add new preview class
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        previewView = findViewById(R.id.previewView);
        cameraProviderFuture = ProcessCameraProvider.getInstance(this);
        textView = findViewById(R.id.orientation);
        cameraProviderFuture.addListener(new Runnable() {
            @Override
            public void run() {
                try {
                    ProcessCameraProvider cameraProvider = cameraProviderFuture.get();
                    bindImageAnalysis(cameraProvider);
                } catch (ExecutionException | InterruptedException | IOException e) {
                    e.printStackTrace();

                }
            }
        }, ContextCompat.getMainExecutor(this));

    }

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    private void bindImageAnalysis(@NonNull ProcessCameraProvider cameraProvider) throws IOException {
        ImageAnalysis imageAnalysis =
                new ImageAnalysis.Builder().setTargetResolution(new Size(1280, 720))
                        .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST).build();
        imageAnalysis.setAnalyzer(ContextCompat.getMainExecutor(this), new ImageAnalysis.Analyzer() {
            @Override
            public void analyze(@NonNull ImageProxy image) {
                image.close();
            }
        });
        OrientationEventListener orientationEventListener = new OrientationEventListener(this) {
            @Override
            public void onOrientationChanged(int orientation) {
                textView.setText(Integer.toString(orientation));
            }
        };
        orientationEventListener.enable();
        Preview preview = new Preview.Builder().build();
        CameraSelector cameraSelector = new CameraSelector.Builder()
                .requireLensFacing(CameraSelector.LENS_FACING_BACK).build();
        preview.setSurfaceProvider(previewView.createSurfaceProvider());
        ImageCapture imageCapture =
                new ImageCapture.Builder().build();
        Camera camera = cameraProvider.bindToLifecycle((LifecycleOwner)this, cameraSelector,imageCapture,
                imageAnalysis, preview);
        verifyStoragePermissions(this);
        //camera.getCameraControl().



    //    camera.getCameraControl().startFocusAndMetering()


        File f;
        try {
//            f = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "DevCameraShare.jpg");
//            boolean isCreated = f.createNewFile();

            f = new File(this.getApplicationContext().getExternalFilesDir("received"),
                    "BackupCamera.jpg");

            File dirs = new File(f.getParent());
            if (!dirs.exists())
                dirs.mkdirs();
            boolean isCreated = f.createNewFile();
            Log.d(MainActivity.TAG, "File Created isCreated = " + isCreated);


            ImageCapture.OutputFileOptions outputFileOptions =
                    new ImageCapture.OutputFileOptions.Builder(f).build();

            previewView.setFocusable(true);
//            val autoFocusPoint = SurfaceOrientedMeteringPointFactory(1f, 1f)
//                    .createPoint(.5f, .5f)

          //  previewView.s
           // camera.getCameraControl().startFocusAndMetering();

//            Handler handler = new Handler();
//            handler.postDelayed(new Runnable() {
//                public void run() {
//                    // yourMethod();
//                }
//            }, 5000);

            //camera.getCameraControl().startFocusAndMetering()

            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    imageCapture.takePicture(outputFileOptions, ContextCompat.getMainExecutor(getBaseContext()),
                            new ImageCapture.OnImageSavedCallback() {
                                @Override
                                public void onImageSaved(ImageCapture.OutputFileResults outputFileResults) {
                                    // insert your code here.
                                    Log.d(MainActivity.TAG, "imageCapture.takePicture onImageSaved " );
                                    //setResult(CameraActivity.RESULT_OK);
                                    finish();
                                }

                                @Override
                                public void onError(ImageCaptureException error) {
                                    // insert your code here.
                                    Log.d(MainActivity.TAG, "ImageCaptureException Message = " + error.getMessage());
                                }
                            }
                    );
                }
            }, 3000);


        }
        catch (IOException e) {
            Log.e(MainActivity.TAG,"Failed to create file");
            Log.e(MainActivity.TAG, e.getMessage());
        }

    }
}